import { UBAH_SIDEBAR } from './types';


export const handleSideBar = (isActive) => dispatch => {
    return dispatch({
        type    : UBAH_SIDEBAR,
        payload : isActive
    });
}