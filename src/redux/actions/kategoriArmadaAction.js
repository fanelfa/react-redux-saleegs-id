import Axios from 'axios'

import {
    FETCH_KATEGORI_ARMADA,
    UPDATE_KATEGORI_ARMADA,
    UPDATE_FAIL_KATEGORI_ARMADA,
    SET_KATEGORI_ARMADA_FORM_VALUE,
    AUTH_ERROR
} from './types'

import { apiUrl } from '../../config/apiUrl';

const tokenConfig = () => {
    // Get token from localstorage
    var token = localStorage.getItem('token');

    // Headers
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    // If token, add to headers
    if (token) {
        config.headers['Authorization'] = "Bearer "+token;
    }

    return config;
};

export const fetchKategoriArmada = () => dispatch => {
    console.log('fetching...')
    Axios.get(apiUrl + 'admin/kategori-armada', tokenConfig())
        .then(res => res.data.data)
        .then(kategoriArmada => dispatch({
            type    : FETCH_KATEGORI_ARMADA,
            payload : kategoriArmada
        }))
        .catch(function (error) {
            // handle error
            console.log(error);
            dispatch({
                type    : AUTH_ERROR
            });
        })
}


export const updateKategoriArmada = ({id, jenis_armada, kapasitas, tahun_armada}) => dispatch => {
    console.log('updating...');
    // Request body
    const body = JSON.stringify({jenis_armada, kapasitas, tahun_armada});

    Axios.put(apiUrl + 'admin/kategori-armada/'+ id, body, tokenConfig())
        .then(res => {
            console.log(res.data);
            return res.data
        })
        .then(response => {
            dispatch({
                type: UPDATE_KATEGORI_ARMADA,
                payload: response.data
            });
        })
        .catch(function (error) {
            dispatch({
                type: UPDATE_FAIL_KATEGORI_ARMADA,
            });
            console.log(error);
        })
}

export const setUpdateFormValue = (data) => dispatch => {
    console.log('send value to form...');
    return dispatch({
        type    : SET_KATEGORI_ARMADA_FORM_VALUE,
        payload : data
    })
}