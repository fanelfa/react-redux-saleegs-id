import { combineReducers } from 'redux';
import authReducer from './authReducer';
import kategoriArmadaReducer from './kategoriArmadaReducer';
import sideBarReducer from './sideBarReducer';

export default combineReducers({
    auth    : authReducer,
    kategoriArmada  : kategoriArmadaReducer,
    sideBar  : sideBarReducer
});