import {
    FETCH_KATEGORI_ARMADA,
    CREATE_KATEGORI_ARMADA,
    SET_KATEGORI_ARMADA_FORM_VALUE,
    UPDATE_KATEGORI_ARMADA,
    UPDATE_FAIL_KATEGORI_ARMADA,
} from '../actions/types';

const initialState = {
    items   : []
};

export default function(state = initialState, action){
    switch(action.type){
        case FETCH_KATEGORI_ARMADA:
            return {
                ...state,
                items: action.payload
            };
        case CREATE_KATEGORI_ARMADA:
            return {
                ...state,
                items: action.payload
            };
        case SET_KATEGORI_ARMADA_FORM_VALUE:
            return {
                ...state,
                formValue: action.payload
            };
        case UPDATE_KATEGORI_ARMADA:
            return {
                ...state,
                items: [action.payload, ...state.items.filter(item => item.id !== action.payload.id)]
            }
        case UPDATE_FAIL_KATEGORI_ARMADA:
            return {
                ...state,
                updated : false,
            }
        default:
            return state;
    }
};