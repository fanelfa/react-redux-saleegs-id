import {
    UBAH_SIDEBAR
} from '../actions/types';

const initialState = {
    isActive: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case UBAH_SIDEBAR:
            return {
                ...state,
                isActive: action.payload
            };
        default:
            return state;
    }
};