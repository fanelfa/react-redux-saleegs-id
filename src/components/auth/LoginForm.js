import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { login } from '../../redux/actions/authAction';

import { Form, Button, Container } from 'react-bootstrap';


class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit(e) {
        e.preventDefault();

        const data = {
            email: this.state.email,
            password: this.state.password,
        }

        this.props.login(data);
    }


    render() {
        return (
            <>
                <Container>
                    <Form onSubmit={this.onSubmit}>
                        <Form.Group controlId="email">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" name="email" onChange={this.onChange} value={this.state.email} placeholder="ex: example@email.com" />
                        </Form.Group>
                        <Form.Group controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" name="password" onChange={this.onChange} value={this.state.password} placeholder="password"/>
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Login
                        </Button>
                    </Form>

                </Container>
            </>
        )
    }
}

LoginForm.propTypes = {
    login: PropTypes.func.isRequired
}


export default connect(null, { login })(LoginForm);