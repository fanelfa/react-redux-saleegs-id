import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';


import { handleSideBar } from '../../redux/actions/sideBarAction';


class ButtonToggle extends Component {
    render() {
        return (
            <Button variant="info" onClick={() =>this.props.handleSideBar(true)}>
                <i className="fas fa-align-left"></i>
                Menu
            </Button>
        )
    }
}

ButtonToggle.propTypes = {
    handleSideBar: PropTypes.func.isRequired,
}


export default connect(null, {handleSideBar})(ButtonToggle);