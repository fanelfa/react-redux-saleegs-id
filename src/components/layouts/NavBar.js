import React, { Component } from 'react';

import { Navbar, Nav, NavDropdown, Container, Button } from 'react-bootstrap';
import { Route, Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import ButtonToggle from './ButtonToggle';
import ModalLogin from '../auth/ModalLogin';
import AboutPage from '../about/AboutPage';
import KategoriArmadaPage from '../kategoriArmada/KategoriArmadaPage';

class MyNavBar extends Component {
    constructor() {
        super();
        this.state = {
            modalLoginShow: true,
            buttonLoginModalShow: true,
        }
    }

    componentDidMount(){
        if(this.props.auth!==null){
            this.setState({
                modalLoginShow: false,
                buttonLoginModalShow: false,
            });
        }
    }

    componentDidUpdate(prevProps){
        if(prevProps.auth !== this.props.auth){
            this.setState({
                modalLoginShow: false,
                buttonLoginModalShow: false,
            });
        }
    }

    render() {
        return (
            <>
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                    <Container>
                        <Navbar.Brand href="#home">Sale ID</Navbar.Brand>
                        < ButtonToggle />
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="mr-auto" activeKey={window.location.pathname}>
                                <Nav.Link componentClass={Link} href="/kategori-armada" to="/kategori-armada">Kategori Armada</Nav.Link>
                                <Nav.Link componentClass={Link} href="/about" to="/about">About</Nav.Link>
                                {/* <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
                                    <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                                    <NavDropdown.Divider />
                                    <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                                </NavDropdown> */}
                            </Nav>
                            <Nav>
                                <Nav.Link href="#deets">More deets</Nav.Link>
                                <Nav.Link eventKey={2} href="#memes">
                                    Dank memes
                                </Nav.Link>
                            {/* Show or hide button login */}
                            {
                                this.state.buttonLoginModalShow ? (
                                    <Button variant="primary" onClick={() => this.setState({
                                        modalLoginShow: true
                                    })}>
                                        Login
                                    </Button>
                                ) :null
                            }
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
                <ModalLogin
                    show={this.state.modalLoginShow}
                    onHide={() => this.setState({
                        modalLoginShow: false
                    })}
                />
                {/* Route Path */}
                <Redirect exact from="/" to="/kategori-armada" />
                <Route path="/about" exact component={AboutPage} />
                <Route path="/kategori-armada" exact component={KategoriArmadaPage} />
            </>
        )
    }
}

const mapStateToProps = state => ({
    auth    : state.auth.access_token
});

export default connect(mapStateToProps)(MyNavBar);