import React, { Component } from 'react';

import {Nav, NavDropdown, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { handleSideBar } from '../../redux/actions/sideBarAction';


class SideBar extends Component{
    render(){
        return(
            <>
                <div className={this.props.isActive ? 'active' : ''} id="overlay" onClick={() => this.props.handleSideBar(false)}></div>
                <Nav className={"col-md-12 bg-dark "+(this.props.isActive?'d-block':'d-none')} id="sidebar"
                >
                    <div id="dismiss">
                        <i className="fas fa-arrow-left"></i>
                        <Button variant="info" onClick={()=>this.props.handleSideBar(false)}>Tutup</Button>
                    </div>

                    <div className="sidebar-header">
                        <h3>Sale ID</h3>
                    </div>
                    <div className="sidebar-sticky"></div>
                    <Nav.Item>
                        <Nav.Link href="/home">Active</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="link-1">Link</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="link-2">Link</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="disabled" disabled>
                            Disabled
                        </Nav.Link>
                    </Nav.Item>
                    <NavDropdown title="Dropdown" id="collasible-nav-dropdown" className="mr-auto">
                        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
            </>
        )
    }
}


SideBar.propTypes = {
    handleSideBar: PropTypes.func.isRequired,
    isActive: PropTypes.bool.isRequired
}


const mapStateToProps = state => ({
    isActive: state.sideBar.isActive
});


export default connect(mapStateToProps, { handleSideBar })(SideBar);