import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Button, Form } from 'react-bootstrap';

import { updateKategoriArmada } from '../../redux/actions/kategoriArmadaAction';


class FormKategoriArmada extends Component {
    constructor(props) {
        super(props);
        this.state = {
            jenis_armada: '',
            kapasitas: '',
            tahun_armada: '',
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount(){
        this.setState(this.props.formValue);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit(e) {
        e.preventDefault();

        const data = {
            id: this.state.id,
            jenis_armada: this.state.jenis_armada,
            kapasitas: this.state.kapasitas,
            tahun_armada: this.state.tahun_armada,
        }

        this.props.updateKategoriArmada(data);
    }


    render() {
        return (
            <>
            <Form onSubmit={this.onSubmit}>
                <Form.Group controlId="jenis_armada">
                    <Form.Label>Jenis Armada</Form.Label>
                    <Form.Control type="text" name="jenis_armada" onChange={this.onChange} value={this.state.jenis_armada} placeholder="ex: Jenis ACD" />
                </Form.Group>
                <Form.Group controlId="kapasitas">
                    <Form.Label>Kapasitas</Form.Label>
                    <Form.Control type="text" name="kapasitas" onChange={this.onChange} value={this.state.kapasitas} placeholder="ex: 200" />
                </Form.Group>
                <Form.Group controlId="tahun_armada">
                    <Form.Label>Tahun Armada</Form.Label>
                    <Form.Control type="text" name="tahun_armada" onChange={this.onChange} value={this.state.tahun_armada} placeholder="ex: 2019" />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
            </>
        )
    }
}

FormKategoriArmada.propTypes = {
    updateKategoriArmada: PropTypes.func.isRequired,
    formValue: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    formValue  : state.kategoriArmada.formValue,
});


export default connect(mapStateToProps, { updateKategoriArmada })(FormKategoriArmada);