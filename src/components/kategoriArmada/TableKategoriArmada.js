import React, { Component } from 'react'

import PropTypes from 'prop-types';
import { connect } from "react-redux";

import { Table, Button } from 'react-bootstrap';

import { fetchKategoriArmada , setUpdateFormValue} from '../../redux/actions/kategoriArmadaAction';
import ModalKategoriArmada from './ModalKategoriArmada';

class TableKategori extends Component{
    constructor() {
        super();
        this.state = {
            modalLoginShow  : false,
        }
    }

    componentDidMount(){
        this.props.fetchKategoriArmada();
    }

    componentDidUpdate(prevProps){
        if(prevProps.auth !== this.props.auth){
            this.props.fetchKategoriArmada();
        }
    }

    render(){
        const kategoriArmadaItems = this.props.kategoriArmada.map(item => (
                <tr key={ item.jenis_armada }>
                    <td>{ item.jenis_armada }</td>
                    <td>{ item.kapasitas }</td>
                    <td>{ item.tahun_armada }</td>
                    <td className="text-center">
                    <Button variant="secondary" size="sm" onClick={() => {
                        this.setState({
                            modalShow   : true,
                        });
                        
                        this.props.setUpdateFormValue({
                            id           : item.id,
                            jenis_armada : item.jenis_armada,
                            kapasitas    : item.kapasitas,
                            tahun_armada : item.tahun_armada,
                        });
                    }
                    }>
                        Edit
                    </Button>{'  '}
                    <Button variant="danger" size="sm">
                        Delete
                    </Button>
                    </td>
                </tr>
            )
            );
        return (
        <>
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th>Jenis</th>
                        <th>Kapasitas</th>
                        <th>Tahun</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    { kategoriArmadaItems }
                </tbody>
            </Table>
            <ModalKategoriArmada
                show={this.state.modalShow}
                onHide={() => this.setState({
                    modalShow: false
                })}
            />
        </>
        );
    }
}

TableKategori.propTypes = {
    fetchKategoriArmada : PropTypes.func.isRequired,
    setUpdateFormValue  : PropTypes.func.isRequired,
    kategoriArmada      : PropTypes.array.isRequired,
    auth                : PropTypes.string,
    updated             : PropTypes.bool,
}

const mapStateToProps = state => ({
    kategoriArmada  : state.kategoriArmada.items,
    auth            : state.auth.access_token,
    updated         : state.kategoriArmada.updated
});


export default connect(mapStateToProps, {fetchKategoriArmada, setUpdateFormValue})(TableKategori);