import React, {Component} from 'react';
import { Container } from 'react-bootstrap';
import TableKategoriArmada from './TableKategoriArmada';


class KategoriArmadaPage extends Component{
 render(){
   return (<div>
     <Container className="container">
       <div className="jumbotron">
         <h1>Kategori Armada</h1>
       </div>
       <TableKategoriArmada />
    </Container>
   </div>)
 } 
}
export default KategoriArmadaPage;