import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from "react-router-dom";

import store from './redux/store';
import NavBar from './components/layouts/NavBar';

// SideBar
import SideBar from './components/layouts/SideBar';


function App() {
  return (
    <Provider store={store}>
      <>
      <Router>
        <NavBar />
        <SideBar/>
      </Router>
      </>
    </Provider>
  );
}

export default App;
